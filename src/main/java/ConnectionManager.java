import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

    private static final String url = "jdbc:mysql://localhost:3306/twittermacron";

    private static final String driver = "com.mysql.cj.jdbc.Driver";

    private static final String user = "root";

    private static final String password = "";

    public ConnectionManager(){};

    public static java.sql.Connection getConnection() {
        java.sql.Connection co = null;

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException el) {
            el.printStackTrace();
        }

        try {
            co = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    return co;}
}
